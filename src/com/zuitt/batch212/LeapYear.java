package com.zuitt.batch212;

import java.util.Scanner;

public class LeapYear {

    public static void main(String[] args) {

        int year;

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter A Year: ");
        year = scan.nextInt();
        boolean isLeapYear = ((year%4==0)&&(year%100!=0)||(year%400==0));

        if(isLeapYear)
        {
            System.out.println(year+ "is Leap Year");
        }else
            System.out.println(year+ "is not a Leap Year");




    }
}
