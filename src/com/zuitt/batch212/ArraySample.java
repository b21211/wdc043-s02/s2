package com.zuitt.batch212;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArraySample {
    public static void main(String[] args) {
        //arrays = fixed/limited collection of data
        // 2^31 = 2, 147,483,648 elements

        //declaration of data type


        int[] intArray = new int[3];
        intArray[0] = 10;
        intArray[1] = 20;
        intArray[2] = 30;

        System.out.println(intArray[2]);
        // other syntax to declare arrays

        int intSample[] = new int[2];
        intSample[0]= 50;
        System.out.println(intSample[0]);
//  stringArray
        String stringArray[]= new String[3];
        stringArray[0]="John";
        stringArray[1]="Ian";
        stringArray[2]="Nics";


        // Declaration with initialization
        int[] intArray2 = {200, 100, 300, 400};

        System.out.println(Arrays.toString(intArray2));
        System.out.println(Arrays.toString(stringArray));

//  Methods used in Arrays
//  sort

        Arrays.sort(stringArray);
        System.out.println(Arrays.toString(stringArray));
//  Binary Search - searches the specified array of the given data type for the Arrays.sort() method

        String searchTerm = "Ian";
        int binaryResult = Arrays.binarySearch(stringArray, searchTerm);
        System.out.println(binaryResult);

//  Multidimensional Array
        String[][] classroom = new String[3][3];
//      first row
        classroom [0][0]="Camp";
        classroom [0][1]="John";
        classroom [0][2]="Hey";
//      second row
        classroom [1][0]="Pik";
        classroom [1][1]="Pak";
        classroom [1][2]="Boom";
//      third row
        classroom [2][0]="This";
        classroom [2][1]="Is";
        classroom [2][2]="Sparta";
        System.out.println(Arrays.deepToString(classroom));

//  HashMap -> KEy: Value Pair
        HashMap<String, String> employeeROle = new HashMap<>();
//  Adding Fields
        employeeROle.put("Captain", "Luffy");
        employeeROle.put("Doctor", "Chopper");
        employeeROle.put("Navigator", "Nami");

        System.out.println(employeeROle);

//  Retrieving Field View
        System.out.println(employeeROle.get("Captain"));

//  Removing Elements


//  Retrieving hashmap keys/fields


//  with Intigers as Value

        HashMap<String, Integer> grades = new HashMap<>();
        grades.put("English", 90);
        grades.put("math", 70);
        System.out.println(grades);

//  Hashmap with Array Lists
        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();

        ArrayList<Integer> gradesListA = new ArrayList<>(Arrays.asList(98,80,86));
        ArrayList<Integer> gradesListB = new ArrayList<>(Arrays.asList(89, 87, 85));

        subjectGrades.put("Joe", gradesListA);
        subjectGrades.put("Jane", gradesListB);
        System.out.println(subjectGrades);




    }
}
